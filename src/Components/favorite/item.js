import React, { Component } from 'react'
import { Card } from 'antd';
import TextTrucate from 'react-text-truncate'
import { connect } from 'react-redux'
const { Meta } = Card

const mapDispatchToProps = dispatch => {
  return {
    onItemMovieClick: item =>
    dispatch({
      type: 'click_item',
      payload: item
    })
  }
}

class ItemFavorite extends Component {
  render() {
    const items = this.props.item
    return (
      <Card
        onClick={() => {
          this.props.onItemMovieClick(items)
        }}
        hoverable
        cover={<img src={items.image_url} style={{ width: '100%' }} />}
      >
        <Meta
          title={items.title}
          description={
            <TextTrucate
              line={1}
              truncateText=".."
              text={items.overview}
              textTruncateChild={<a href=" ">Read more</a>}
            />
          }
        />
      </Card>
    )
  }
}

export default  connect(
  null,
  mapDispatchToProps //#endregion
)(ItemFavorite)