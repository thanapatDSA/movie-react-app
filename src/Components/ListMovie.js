import React from 'react'
import { List } from 'antd';
import ItemMovie from './ItemMovie'

function ListMovie(props){
  
  // console.log('items : ',props.items)
  return(
    <List
    grid={{ gutter: 16, column : 4 }}
    dataSource={props.items}
    renderItem={item => (
      <List.Item>
        <ItemMovie item = {item} />
      </List.Item>
    )}
    />
  )
}

export default ListMovie;