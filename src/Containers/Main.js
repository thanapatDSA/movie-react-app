import { message, Menu, Button, Spin, Modal, Layout } from 'antd';
import React, { Component } from 'react';
// import ListMovie from '../Components/ListMovie';
import RouteMenu from './RouteMenu'
import {connect} from 'react-redux'
const { Header, Content, Footer } = Layout
const menus = ['movies', 'favorite', 'profile'];

const mapStateToProps = state => {
  console.log('Main resucd:',state)
  return{
    isShowDialog: state.isShowDialog,
    itemMovieClick: state.itemMovieDetail
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onDismissDialog: () =>
    dispatch({
      type: 'dismiss_dialog'
    }),
    onItemMovieClick: item =>
    dispatch({
      type: 'click_item',
      payload: item
    })
    }
  }

class Main extends Component {

  state = {
    items: [],
    isShowModal: false,
    itemMovie: null,
    pathName: menus[0],
    favItem: []
  }
  

  onMenuClick = (e) => {
    var path = '/'
    if (e.key !== 'home') {
      path = `/${e.key}`
    }
    this.props.history.replace(path)
  }
  onClickFavorite = () => {
    const itemClick = this.props.itemMovieClick
    const items = this.state.favItem
    const result = items.find(items => {
      return items.title === itemClick.title
    })
    if (result) {
      message.error('This item is added');
    } else {
      items.push(itemClick)
      localStorage.setItem('list-fav', JSON.stringify(items))
      console.log(items)
      message.success('Saved your favorite movie', 1)
      this.onModalClicCancel()
    }

  }
  onModalClickOk = () => {
    this.props.onDismissDialog()
  }
  onModalClicCancel = () => {
    this.props.onDismissDialog()
  }
  componentDidMount() {
    const jsonStr = localStorage.getItem('list-fav')
    if (jsonStr) {
      const items = jsonStr && JSON.parse(jsonStr)
      console.log(items)
      this.setState({ favItem: items })
    }
    const { pathname } = this.props.location
    var pathName = menus[0]
    if (pathname !== '/') {
      pathName = pathname.replace('/', '')
      if (!menus.includes(pathName)) pathName = menus[0]
    }
    this.setState({ pathName })
    fetch('http://workshopup.herokuapp.com/movie')
      .then(response => response.json())
      .then(movies => this.setState({ items: movies.results }))
  }
  render() {
    const item = this.props.itemMovieClick
    return (
      <div>
        {this.state.items.length > 0 ? (
          <div style={{ height: '100' }}>
            {' '}
            <Layout className="layout" style={{ background: 'white' }}>
              <Header
                style={{
                  padding: '0px',
                  position: 'fixed',
                  zIndex: 1,
                  width: '100%',
                  background: 'white'
                }}
              >
                <Menu
                  theme="light"
                  mode="horizontal"
                  defaultSelectedKeys={[this.state.pathName]}
                  style={{ lineFeight: '64px', background: 'white' }}
                  onClick={e => {
                    this.onMenuClick(e);
                  }}
                >
                  <Menu.Item key={menus[0]}>Home</Menu.Item>
                  <Menu.Item key={menus[1]}>Favorite</Menu.Item>
                  <Menu.Item key={menus[2]}>Profile</Menu.Item>
                </Menu>
              </Header>
              <Content
                style={{
                  padding: '16px',
                  marginTop: 64,
                  minHright: '600px',
                  justifyContent: 'center',
                  alignItems: 'center',
                  display: 'flex',
                  background: '#d5d5d5'
                }}
              >
                <RouteMenu
                  items={this.state.items}
                  // onItemMovieClick={this.onItemMovieClick}
                />
              </Content>
              <Footer style={{ textAlign: 'center', background: 'white' }}>
                Movie Application Workshop @ CAMT
                </Footer>
            </Layout>
            {/* <ListMovie items={this.state.items} onItemMovieClick={this.onItemMovieClick} /> */}
          </div>) : (
            <Spin size='large' />
          )}
        {item != null ? (
          <Modal
            width="40%"
            style={{ maxHeight: '70%' }}
            title={item.title}
            visible={this.props.isShowDialog}
            onOk={this.onModalClickOk}
            onCancel={this.onModalClicCancel}
            footer={[
              <Button
                key="submit"
                type='primary'
                icon="shopping-cart"
                size='large'
                shape="circle"
                onClick={this.onClickFavorite}
              />,
            ]}
          >
            <img src={item.image_url} style={{ width: '100%' }} />
            <br />
            <br />
            <p>{item.overview}</p>
          </Modal>

        ) : (
            <div />
          )}
      </div>

    )

  }
}


export default connect(
  mapStateToProps,
  mapDispatchToProps //#endregion
)(Main)
