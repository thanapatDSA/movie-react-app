import React from 'react';
import ListMovie from '../Components/ListMovie'
import ListFavorite from '../Components/favorite/list'
import Profile from '../Components/profile/index'
import { Route, Switch, Redirect } from 'react-router-dom';

function RouteMenu(props) {
  // console.log(props.items)
  return (
    <Switch>
      <Route
        path="/movies"
        exact
        render={() => {
          return (
            <ListMovie
              items={props.items}
              onItemMovieClick={props.onItemMovieClick}
            />
          )
        }}
      />
      <Route path="/favorite" exact component={ListFavorite} />
      <Route path="/profile" exact component={Profile} />
      <Redirect from="/*" exact to="/" />
    </Switch>
  )
}

export default RouteMenu